import { Button, Form, Input } from 'antd';



export const Login = () => {

    const [form] = Form.useForm();

    const onFinish = (values) => {
        console.log(values);
    };


    return <>
        <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            style={{ maxWidth: 600 }}
            form={form}

            onFinish={onFinish}

            autoComplete="off"
        >
            <Form.Item
                label="email"
                name="email"
                rules={[{ required: true, message: 'Por favor ingresa tu email!' }]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[{ required: true, message: 'Por favor ingresa tu  password!' }]}
            >
                <Input.Password />
            </Form.Item>



            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>

    </>
};
