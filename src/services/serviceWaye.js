import axios from 'axios';
const url = require("../config/config").server.wayeBackend.url

export function getItem2(idItem) {
    axios.get(`${url}/aticulo/1`)
        .then(response => {
            // Handle response
            console.log(response.data);
        })
        .catch(err => {
            // Handle errors
            console.error(err);
        });
}

export const getItem = async (idItem) => {
    const response = await axios.get(
        `${url}/aticulo/${idItem}`
    );
    return response;
};