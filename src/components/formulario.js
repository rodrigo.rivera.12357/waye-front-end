import React, { useState } from 'react';
import { Input, Switch, Popconfirm, Table, Button, Card, InputNumber, Select, Modal } from 'antd';
import { getItem } from '../services/serviceWaye'
import { QrReader } from 'react-qr-reader';



const { Search } = Input;
//import { ScannerQr } from "../components/scanner"

export const FormularioVenta = () => {

    const [valor, setValor] = useState('');
    const [viewCamera, setViewCamera] = useState(false);
    const [id, setId] = useState("");
    const [nombre, setNombre] = useState("");
    const [cantidad, setCantidad] = useState("");
    const [precioN, setPrecioN] = useState("");
    const [precioM, setPrecioM] = useState("");
    const [tipoVenta, setTipoVenta] = useState(false);
    const [cantidadVenta, setCantidadVenta] = useState();
    const [botonBuscar, setBotonBuscar] = useState(true);
    const [butonEnviar, setButonEnviar] = useState(true);
    const [envioDinero, setEvnioDinero] = useState(40);
    const [entregas, setEntrega] = useState("mercadoLibre");
    const [textoModal, setTextoModal] = useState("");
    const [total, setTotal] = useState(0);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [resultQr, setResultQr] = useState();

    const defaultColumns = [{
        title: 'Id',
        dataIndex: 'id',
    },
    {
        title: 'Nombre',
        dataIndex: 'nombre',
        width: '30%',
    },
    {
        title: 'Stock',
        dataIndex: 'stock',
    },
    {
        title: 'Precio Mayoreo',
        dataIndex: 'precioM',
    },
    {
        title: 'Precio',
        dataIndex: 'precioN',
    },
    {
        title: 'cantidad',
        dataIndex: 'cantidadVenta',
    },
    {
        title: 'total',
        dataIndex: 'total',
    },
    {
        title: 'operation',
        dataIndex: 'operation',
        render: (_, record) =>
            dataSource.length >= 1 ? (
                <Popconfirm title="Seguro eliminar?" onConfirm={() => handleDelete(record.key)}>
                    <a href={() => false}>Eliminar</a>
                </Popconfirm>
            ) : null,
    },
    ];
    const [dataSource, setDataSource] = useState([

    ]);

    const columns = defaultColumns.map((col) => {
        if (!col.editable) {
            return col;
        }
        return {
            ...col,
            onCell: (record) => ({
                record,
                editable: col.editable,
                dataIndex: col.dataIndex,
                title: col.title,
                handleSave,
            }),
        };
    });

    const handleSave = (row) => {
        const newData = [...dataSource];
        const index = newData.findIndex((item) => row.key === item.key);
        const item = newData[index];
        newData.splice(index, 1, {
            ...item,
            ...row,
        });
        setDataSource(newData);
    };
    const handleDelete = (key) => {
        const newData = dataSource.filter((item) => item.key !== key);
        setDataSource(newData);
        let tempTotal = 0;
        newData.forEach(t => { tempTotal += t.total; })

        setTotal(tempTotal)
        newData.length === 0 ? setButonEnviar(true) : setButonEnviar(false)


    };


    const handleChange = (event) => { setValor(event.target.value) }

    const onBuscar = data => {
        console.log("data", data == null)
        if (data.length !== 0) {
            console.log("entre")
            getItem(data).then(response => {
                console.log("response ", response.data.data);
                setCantidadVenta(1); // default                
                setId(response.data.data.id);
                setNombre(response.data.data.nombre);
                setCantidad(response.data.data.cantidad);
                setPrecioN(response.data.data.precioP);
                setPrecioM(response.data.data.precioM);
            }).catch(response => {

                console.log("error ", response.response.data);
                setIsModalOpen(true)
                setTextoModal(response.response.data.mensaje)
                dafultData();
            });
            setBotonBuscar(false)

        } else {
            dafultData();
        }
    }

    const dafultData = () => { setCantidadVenta(); setId(); setNombre(); setCantidad(); setPrecioN(); setPrecioM(); setBotonBuscar(true); }
    const camara = data => { setViewCamera(data) }

    const onTipoVenta = data => { setTipoVenta(data); console.log("biusca", botonBuscar, "cantidadVenta", cantidadVenta) }

    const onCantidad = data => {

        console.log("onCantidad ", data)
        if (data === null) {
            console.log("no se pude habilitar", botonBuscar);
            setBotonBuscar(true)

        }
        if (data !== null && botonBuscar) {
            console.log("se puede habilitar", botonBuscar);
            setBotonBuscar(false)

        }
        setCantidadVenta(data)

    }

    const addTable = () => {
        let temp = {
            key: new Date(), id, nombre, stock: cantidad, precioM, precioN, cantidadVenta,
            total: tipoVenta ? (parseInt(cantidadVenta, 10) * parseInt(precioM, 10)) : (parseInt(cantidadVenta, 10) * parseInt(precioN, 10))
        }
        setDataSource(dataSource.concat(temp));
        let tempTotal = 0;
        dataSource.forEach(t => { tempTotal += t.total; })
        tempTotal += temp.total // suma el ultimop
        setTotal(tempTotal)
        setButonEnviar(false)


    }
    const tipoDeEntrega = (data) => {
        setEntrega(data)
    }
    const onDineroEnvio = data => { setEvnioDinero(data) }

    const enviar = () => {
        console.log(entregas)
        let temp = dataSource.map(t => {
            return {
                "productos": [
                    {
                        "idArticulo": t.id,
                        "precio": t.total,
                        "cantidad": t.cantidadVenta
                    }
                ]
            }
        });
        temp.envio = envioDinero;
        temp.tipoVenta = entregas;
        console.log("enviar ", temp)
    }

    const handleOk = () => { setIsModalOpen(false); };


    return <>

        {viewCamera ?
            <Card hidde="true" title="Leer Qr" bordered={false} style={{ width: 300 }}>
                <QrReader onResult={(result, error) => {

                    if (!!result) {
                        setResultQr(result?.text);
                        setId(result?.text);
                    }
                    if (!!error) {

                    }
                }}
                    style={{ width: '100%' }}
                />
            </Card> :
            <>hola mundo</>}

        {resultQr}

        <Switch onChange={camara} />

        <br />
        <Search
            placeholder="introduce el id "
            allowClear
            enterButton="Buscar"
            size="large"
            onChange={handleChange}
            onSearch={onBuscar}
            disabled={viewCamera}
            value={valor}

            style={{ width: 300 }}
        />

        <div >
            <h4>Nombre:</h4><Input disable="true" value={nombre} style={{ width: 300 }} />
            <h4>Stock:</h4><Input disable="true" value={cantidad} style={{ width: 300 }} />
            <h4>Precio Normal:</h4>    <Input disable="true" value={precioN} style={{ width: 300 }} />
            <h4>Precio Mayoreo:</h4><Input disable="true" value={precioM} style={{ width: 300 }} />
            <h4>Cantidad:</h4><InputNumber onChange={onCantidad} min="0" max="100" value={cantidadVenta} style={{ width: 300 }} />
            <h4>Mayoreo</h4> <Switch onChange={onTipoVenta} />


            <Button disabled={botonBuscar} onClick={addTable} type="primary" style={{ width: 300 }} >Agregar</Button>
        </div>

        <Table
            bordered
            dataSource={dataSource}
            columns={columns}
        />
        <br />
        <h4>Sub Total:</h4><InputNumber disabled="true" value={total} min="10" max="100" style={{ width: 300 }} />

        <br />
        <h4>envio:</h4><InputNumber onChange={onDineroEnvio} value={envioDinero} min="0" max="100" style={{ width: 300 }} />
        <br />
        <Select
            defaultValue="mercadoLibre"
            style={{ width: 200 }}
            onChange={tipoDeEntrega}
            options={[
                { value: 'mercadoLibre', label: 'Mercado Libre' },
                { value: 'metro', label: 'Metro' },
                { value: 'personal', label: 'Entrega Personal' },
            ]}
        />

        <br />
        <Button disabled={butonEnviar} onClick={enviar} type="primary" style={{ width: 300 }} >Enviar</Button>

        <Modal title="Mensaje" open={isModalOpen} onOk={handleOk} onCancel={handleOk}>
            <p>{textoModal}</p>
        </Modal>
    </>
}